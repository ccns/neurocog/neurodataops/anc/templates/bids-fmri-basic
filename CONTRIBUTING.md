# Contributing

Issues that should carry over to the user template should be provided with a scoped label `ANC Task`, with an identifier for the specific issue. Use the issue number in the metatemplate as an identifier.

## Generate template project

To create the template project use the [Gitlab project template generator](https://gitlab.com/ccns/neurocog/neurodataops/anc/software/gitlab-project-template-generator)

Follow documentation to install and run.

Specify the following in the configuration file:

### Pheno-only dataset

Copy over the following files from the meta-template:

```yaml
filenames:
  - '.bids-validator-config.json'
  - '.gitignore'
  - '.bidsignore'
  - '.gitattributes'
  - '.gitlab-ci.yml'
  - 'CITATION.cff'
  - 'README_pheno.md'
  - 'dataset_description.json'
  - 'participants.json'
  - 'participants.tsv'
  - 'phenotype/measurement_tool_name.tsv'
  - 'phenotype/measurement_tool_name.json'
renames:
  'README_pheno.md': 'README.md'
issue_labels:
  - 'ANC Task::45'
  - 'ANC Task::9'
  - 'ANC Task::8'
  - 'ANC Task::7'
  - 'ANC Task::6'
  - 'Dataset status::Initialized'
```

### Other dataset 

Copy over the following files from the meta-template:

```yaml
filenames:
  - '.bids-validator-config.json'
  - '.gitignore'
  - '.bidsignore'
  - '.gitattributes'
  - '.gitlab-ci.yml'
  - 'CITATION.cff'
  - 'README_neuroimaging.md'
  - 'dataset_description.json'
  - 'participants.json'
  - 'participants.tsv'
renames:
  'README_neuroimaging.md': 'README.md'
issue_labels:
  - 'ANC Task::9'
  - 'ANC Task::8'
  - 'ANC Task::7'
  - 'ANC Task::6'
  - 'ANC Task::12'
  - 'ANC Task::45'  
  - 'Dataset status::Initialized'
```

## Correct settings

After the template file has been generated, certain settings need to be updated, specifically:

- Set the project visibility to internal so that people can see and use the template

## Labels 

Make sure that labels that get assigned in the `issue_labels` section are available in the [`BIDS Datasets`](https://data.anc.plus.ac.at/bids-datasets) group. Otherwise the labels will be created on project level and querying for them is not possible on the group level. 
