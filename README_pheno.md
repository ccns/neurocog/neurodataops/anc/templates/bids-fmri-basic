> This template README is a work in progress. It is a modified version of the [BIDS README template](https://github.com/bids-standard/bids-starter-kit/blob/main/templates/README.MD). Adapt it to your needs.

> The README is usually the starting point for researchers using your data and serves as a guidepost for users of your data. A clear and informative README makes your data much more usable.
In general you can include information in the README that is not captured by some other files in the BIDS dataset (`dataset_description.json`, `*_events.json`, etc.).
It can also be useful to include information that might already be present in another file of the dataset but might be important for users to be aware of before using the data.

A short paragraph giving an overview of the dataset. This should include the reason the goals and purpose of the dataset and a discussion of how the dataset tries to achieve these goals. Additionally, elements that make this dataset unique and particularly valueable can be highlighted here.

## Overview

### Description of the contents of the dataset

Description of what type of data, what tasks and the number of subject one can expect to find in the dataset.

### Independent variables

A list of condition variables (sometimes called contrasts or independent variables) that were varied across the experiment.

- healthy controls  vs. patients with major depressive disorder
- pleasant vs. unpleasent images
- nouns vs. verbs
- houses vs. faces

### Dependent variables

A list of the response variables (sometimes called the dependent variables) that were measured and or calculated to assess the effects of varying the condition variables. This might also include questionnaires administered to assess behavioral aspects of the experiment.

- reaction times
- BOLD signal
- task performance

### Control variables

List of the control variables - that is what aspects were explicitly controlled in this experiment. The control variables might include subject pool, environmental conditions, set up, or other things that were explicitly controlled.

- age
- sex
- familiarity of images
- length of words
- reading performance

### Quality assessment of the data

Link to the quality report of the data in the derivatives.

## Methods

### Subjects

A brief description of the subject pool in this experiment, including:
- information about the recruitment procedure,
- subject inclusion criteria (if relevant),
- subject exclusion criteria (if relevant).

### Apparatus

A summary of the equipment and environment setup for the experiment. For example, was the experiment performed in a shielded room with the subject seated in a fixed position.

### Initial setup

A summary of what setup was performed when a subject arrived.

### Task organization

How the tasks were organized for a session. This is particularly important because BIDS datasets usually have task data separated into different files.

Consider the following questions:
- Was task order counter-balanced?
- What other activities were interspersed between tasks?
- In what order were the tasks and other activities performed?

### Task details

As much detail as possible about the tasks and the events that were recorded.

### Additional data acquired

A brief indication of data other than the imaging data that was acquired as part of this experiment. In addition to data from other modalities and behavioral data, swabs, and clinical information. Indicate the availability of this data.

This is especially relevant if the data are not included in a [`phenotype` folder](https://bids-specification.readthedocs.io/en/stable/03-modality-agnostic-files.html#phenotypic-and-assessment-data).

### Experimental location

This should include any additional information regarding the geographical location and facility that cannot be included in the relevant json files.

### Missing data

Mention something if some participants are missing some aspects of the data. We recommend listing missing files by subject, and abnormalities in recorded data by filename.

Some examples of missing/deviating data:
- A brain lesion or defect only present in one participant.
- Some experimental conditions missing on a given run for a participant because of some technical issue.
- Any noticeable feature of the data for certain participants.
- Differences (even slight) in protocol for certain participants.

Missing files

| Subject | Missing files |
| ------ | ------ |
| `sub-s003/` | `BDI` |

Known issues in collected data

| File | Known issues |
| ------ | ------ |
| `sub-s003` | Filled in only half of the questionnaires |
| `sub-s012` | Abort of testing |


### Notes

Any additional information or pointers to information that might be helpful to users of the dataset. Include qualitative information related to how the data acquisition went.

## Data access and reuse

### Data user agreement

If the dataset requires a data user agreement, link to the relevant information.

### Contact person

Indicate the name and contact details (email and ORCID) of the person responsible for additional information.

## References

List of references used within the README.
