# Meta template project for the ANC templates

This project is the source for the ANC project templates.

The ANC project templates are generated with CI/CD of this project.

The project generation is done with [GitLab Project Template Generator](https://gitlab.com/ccns/neurocog/neurodataops/anc/software/gitlab-project-template-generator/).

The generation needs access tokens to this project and to the target ANC instance. For this project use Project Access Token (`read_api` scope, Guest role). For the target use the access token of the [ACN Template Operator service user](https://gitlab.com/ccns/neurocog/neurodataops/anc/documentation/anc-admin-docs/-/blob/main/gitlab/service_users.md?ref_type=heads#list-of-service-accounts) (`api` scope, Owner role in the template group). Set the tokens as CI/CD variables, `SOURCE_AT` and `ANC_AT` (masked and hidden).